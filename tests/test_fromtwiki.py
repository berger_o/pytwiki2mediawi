from fromtwiki import translateText, convertedFromStream
import unittest
import StringIO

class TestTranslationRules(unittest.TestCase):

    def test_TOC_removal(self):
        mw = translateText('%TOC%')
        self.assertEqual(mw, '')

    def test_META_removal(self):
        mw = translateText('%META djfgskjdfg%')
        self.assertEqual(mw, '')

    def test_bold(self):
        mw = translateText('Hi this *is a bold* string')
        self.assertEqual(mw,"Hi this '''is a bold''' string")

    def test_fullbold(self):
        mw = translateText('*Lazy Fox are you there ?*')
        self.assertEqual(mw,"'''Lazy Fox are you there ?'''")

    def test_italicbold(self):
        mw = translateText('Hi this is __a bold italic__ one')
        self.assertEqual(mw,"Hi this is ''<b>a bold italic</b>'' one")

    def test_italic(self):
        mw = translateText('And This is _an italic_ one')
        self.assertEqual(mw,"And This is ''an italic'' one")

    def test_monospacedbold(self):
        mw = translateText('And this is a ==monospaced bold== one')
        self.assertEqual(mw,"And this is a '''<tt>monospaced bold</tt>''' one")

    def test_monospaced(self):
        mw = translateText('And this is =a monospaced= one')
        self.assertEqual(mw,"And this is <tt>a monospaced</tt> one")

    def test_h6(self):
        mw = translateText('---++++++ H6')
        self.assertEqual(mw,"====== H6 ======")

    def test_h5(self):
        mw = translateText('---+++++ H5')
        self.assertEqual(mw,"===== H5 =====")

    def test_h4(self):
        mw = translateText('---++++ H4')
        self.assertEqual(mw,"==== H4 ====")

    def test_h3(self):
        mw = translateText('---+++ H3')
        self.assertEqual(mw,"=== H3 ===")

    def test_h2(self):
        mw = translateText('---++ H2')
        self.assertEqual(mw,"== H2 ==")

    def test_h2malformed(self):
        mw = translateText('----++ H2 (slightly misformed variant)')
        self.assertEqual(mw,"== H2 (slightly misformed variant) ==")

    def test_h1(self):
        mw = translateText('---+ H1')
        self.assertEqual(mw,"= H1 =")

    def test_h1notoc(self):
        mw = translateText('---+!! H1')
        self.assertEqual(mw,"= H1 =")

    def test_linkexternal(self):
        mw = translateText('an [[http://example.com/][external link]]')
        self.assertEqual(mw,'an [http://example.com/ external link]')

    def test_linkexternalcamelcaseindesc(self):
        mw = translateText('an [[http://example.com/][CamelCase external link]]')
        self.assertEqual(mw,'an [http://example.com/ CamelCase external link]')

    def test_linkexternalcamelcaseinurl(self):
        tw = 'an http://example.com/index.php/CamelCase link'
        mw = translateText(tw)
        self.assertEqual(mw, tw)
        
    def test_camelcaselinkinternal(self):
        mw = translateText('This is a TestCase of a VerySimple CamelCaseParser.')
        self.assertEqual(mw,'This is a [[TestCase]] of a [[VerySimple]] [[CamelCaseParser]].')

    def test_camelslashcase(self):
        mw = translateText('This is a Test/Case of a VerySimple CamelCaseParser.')
        self.assertEqual(mw,'This is a Test/Case of a [[VerySimple]] [[CamelCaseParser]].')

    def test_prefixedcamelcaselinkinternal(self):
        mw = translateText('Learn how to edit in : Sandbox.WebHome')
        self.assertEqual(mw, 'Learn how to edit in : Sandbox.WebHome')

    def test_uppercasetext(self):
        tw = 'TEST OF UPPERCASED text WORKING'
        mw = translateText(tw)
        self.assertEqual(mw, tw)
    
    def test_linkinternalexplicit(self):
        tw = '[[WikiWord]]'
        mw = translateText(tw)
        self.assertEqual(mw, tw)

    def test_linkinternalexplicitanchor(self):
        tw = '[[WikiWord#TheSyntax]]'
        mw = translateText(tw)
        self.assertEqual(mw, tw)

    def test_linkinternalexplicitaliased(self):
        tw = '[[WikiSyntax][wiki syntax]]'
        mw = translateText(tw)
        self.assertEqual(mw, '[[WikiSyntax|wiki syntax]]')

    def test_bullet1(self):
        mw = translateText('   * level 1 bullet')
        self.assertEqual(mw,'* level 1 bullet')

    def test_bullet1singletab(self):
        mw = translateText("\t* level 1 bullet: Handle single tabs (from twiki .txt files)")
        self.assertEqual(mw,'* level 1 bullet: Handle single tabs (from twiki .txt files)')

    def test_bullet2(self):
        mw = translateText('      * level 2 bullet')
        self.assertEqual(mw,'** level 2 bullet')

    def test_bullet1doubletab(self):
        mw = translateText("\t\t* level 2 bullet: Handle double tabs")
        self.assertEqual(mw,'** level 2 bullet: Handle double tabs')

    def test_bullet3(self):
        mw = translateText('         * level 3 bullet')
        self.assertEqual(mw,'*** level 3 bullet')

    def test_bullet3tabbed(self):
        mw = translateText("\t\t\t* level 3 bullet: Handle tabbed version")
        self.assertEqual(mw,'*** level 3 bullet: Handle tabbed version')

    def test_bullet4(self):
        mw = translateText('            * level 4 bullet')
        self.assertEqual(mw,'**** level 4 bullet')

    def test_bullet5(self):
        mw = translateText('               * level 5 bullet')
        self.assertEqual(mw,'***** level 5 bullet')

    def test_bullet6(self):
        mw = translateText('                  * level 6 bullet')
        self.assertEqual(mw,'****** level 6 bullet')

    def test_bullet7(self):
        mw = translateText('                     * level 7 bullet')
        self.assertEqual(mw,'******* level 7 bullet')

    def test_bullet8(self):
        mw = translateText('                        * level 8 bullet')
        self.assertEqual(mw,'******** level 8 bullet')

    def test_bullet9(self):
        mw = translateText('                           * level 9 bullet')
        self.assertEqual(mw,'********* level 9 bullet')

    def test_bullet10(self):
        mw = translateText('                              * level 10 bullet')
        self.assertEqual(mw,'********** level 10 bullet')

    def test_numbered1(self):
        mw = translateText('   1. level 1 numbered')
        self.assertEqual(mw,'# level 1 numbered')

    def test_numbered1tabb(self):
        mw = translateText("\t1. level 1 numbered: Handle single tabs (from twiki .txt files)")
        self.assertEqual(mw,'# level 1 numbered: Handle single tabs (from twiki .txt files)')

    def test_numbered2(self):
        mw = translateText('      1. level 2 numbered')
        self.assertEqual(mw,'## level 2 numbered')

    def test_numbered2tabbed(self):
        mw = translateText("\t\t1 level 2 numbered: Handle double tabs")
        self.assertEqual(mw,'## level 2 numbered: Handle double tabs')

    def test_numbered3(self):
        mw = translateText('         1. level 3 numbered')
        self.assertEqual(mw,'### level 3 numbered')

    def test_numbered3tabbed(self):
        mw = translateText("\t\t\t1 level 3 numbered: Handle tabbed version")
        self.assertEqual(mw,'### level 3 numbered: Handle tabbed version')

    def test_numbered4(self):
        mw = translateText('            1. level 4 numbered')
        self.assertEqual(mw,'#### level 4 numbered')

    def test_numbered5(self):
        mw = translateText('               1. level 5 numbered')
        self.assertEqual(mw,'##### level 5 numbered')

    def test_numbered6(self):
        mw = translateText('                  1. level 6 numbered')
        self.assertEqual(mw,'###### level 6 numbered')

    def test_numbered7(self):
        mw = translateText('                     1. level 7 numbered')
        self.assertEqual(mw,'####### level 7 numbered')

    def test_numbered8(self):
        mw = translateText('                        1. level 8 numbered')
        self.assertEqual(mw,'######## level 8 numbered')

    def test_numbered9(self):
        mw = translateText('                           1. level 9 numbered')
        self.assertEqual(mw,'######### level 9 numbered')

    def test_numbered10(self):
        mw = translateText('                              1. level 10 numbered')
        self.assertEqual(mw,'########## level 10 numbered')

    def test_definitionterm(self):
        mw = translateText('   $ Sushi: Japan')
        self.assertEqual(mw,'; Sushi : Japan')

    def test_spacesremoval(self):
        mw = translateText('    skhlkshf')
        self.assertEqual(mw,'skhlkshf')

    def test_verbatim(self):
        input = StringIO.StringIO("""<verbatim>
plop
</verbatim>
""")
        mw = convertedFromStream(input)
        self.assertEqual(mw,""" <nowiki>\nplop\n</nowiki>\n""")

    def test_verbatimCamelCase(self):
        input = StringIO.StringIO("""<verbatim>
CamelCase=0
</verbatim>
""")
        mw = convertedFromStream(input)
        self.assertEqual(mw, """ <nowiki>\nCamelCase=0\n</nowiki>\n""")

    def test_table(self):
        mw = translateText("""
| *L1* | *C* | *R* |
| A2 |  B2  |  C2 |
| A3 |  B3  |  C3 |""")
        self.assertEqual(mw,"| '''L1''' | '''C* | *R''' |\n| A2 |  B2  |  C2 |\n| A3 |  B3  |  C3 |")

    def test_nowikiword(self):
        mw = translateText('''a not !TwikiWord''')
        self.assertEqual(mw, 'a not TwikiWord')

    def test_searchmacro(self):
        mw = translateText('''%SEARCH{".*" web="%INCLUDINGWEB%" regex="on" nosearch="on" nototal="on" order="modified" limit="20" reverse="on"  format="| [[$topic][$topic(25, ...)]] | $wikiusername  | $date |" }%''')
        self.assertEqual(mw, '')

    def test_camelcaseemail(self):
        tw = 'an Foo.Bar@example.com email'
        mw = translateText(tw)
        self.assertEqual(mw, tw)

    def test_attachurl(self):
        tw = '[[%ATTACHURL%/a-file_attached.pdf]]'
        mw = translateText(tw, topicname='AnotherPage')
        self.assertEqual(mw, '[[Media:AnotherPage_a-file_attached.pdf]]')

    def test_attachurlwithtitle(self):
        tw = '[[%ATTACHURL%/afile.pdf][a PDF file]]'
        mw = translateText(tw, topicname='AnotherPage')
        self.assertEqual(mw, '[[Media:AnotherPage_afile.pdf|a PDF file]]')

    def test_attachfilecamelcasename(self):
        tw = '[[%ATTACHURL%/a-camelcase-file.pdf][a-CamelCase-file.pdf]]'
        mw = translateText(tw, topicname='AnotherPage')
        self.assertEqual(mw, '[[Media:AnotherPage_a-camelcase-file.pdf|a-CamelCase-file.pdf]]')

    def test_attachfilecamelcasenameinattachfilename(self):
        tw = '[[%ATTACHURL%/CamelCase.pdf][camelcase.pdf]]'
        mw = translateText(tw, topicname='AnotherPage')
        self.assertEqual(mw, '[[Media:AnotherPage_CamelCase.pdf|camelcase.pdf]]')

    def test_attachfilecamelcasenameeverywhere1(self):
        tw = '[[%ATTACHURL%/CamelCase.pdf][a-CamelCase.pdf]]'
        mw = translateText(tw, topicname='AnotherPage')
        self.assertEqual(mw, '[[Media:AnotherPage_CamelCase.pdf|a-CamelCase.pdf]]')

    def test_attachfilecamelcasenameeverywhere2(self):
        tw = '[[%ATTACHURL%/CamelCase.pdf][CamelCase.pdf]]'
        mw = translateText(tw, topicname='AnotherPage')
        self.assertEqual(mw, '[[Media:AnotherPage_CamelCase.pdf|CamelCase.pdf]]')

    def test_scripturlview(self):
        tw = '[[%SCRIPTURL{"view"}%/Awiki/Subwiki/TopicName][whatever]]'
        mw = translateText(tw)
        self.assertEqual(mw, '[[TopicName|whatever]]')
#

