
# Library of routines to export data from a TWiki instance

# Copyright 2014 Olivier Berger <olivier.berger@telecom-sudparis.eu> and Institut Mines-Telecom

# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from bs4 import BeautifulSoup
import posixpath
import requests
import urlparse
import urllib
import sys
import re
import logging

from httpcache import CachingHTTPAdapter

ReservedTWikiWikiTopics = ['WebTopicList', 'WebSearch', 'WebChanges', 'WebNotify', 'WebStatistics', 'WebPreferences', 'WebRss', 'WebAtom', 'WebIndex', 'WebLeftBar', 'WebSearchAdvanced', 'WebTopicCreator']

user = password = None
verifySSL=True

session = None

def fetchTopicHtmlView(topicUrl):
    
    global session

    rawMode = False
    printableMode = False

    if not session:
        session = requests.session()
        session.mount('http://', CachingHTTPAdapter())
        session.mount('https://', CachingHTTPAdapter())
    s = session

    r = s.get(topicUrl, verify=verifySSL)

    if r.status_code == 200:
        #print 'fetched', r.url
        #print r.text

        o = urlparse.urlparse(topicUrl)
        origUrl = o.path
        # handle requests for raw pages : ?raw=on
        if o.query and o.query == 'raw=on':
            rawMode = True
            origUrl = origUrl + '?rawon'
        if o.query and o.query == 'template=viewprint':
            printableMode = True
            origUrl = origUrl + '?templateviewprint'

        #print origUrl

        soup = BeautifulSoup(r.text)

        loginform = soup.find('form', {'name' : "loginform"})

        # check if there's a loginform
        if loginform :

            if not user or not password:
                print "error: need user and password"
                sys.exit(1)

            action = loginform['action']
            #print action

            loginURL = urlparse.urljoin(topicUrl, action)

            headers = {'User-Agent': 'My User Agent 1.0', 'Referer': topicUrl}

            payload = {'username': user, 'password': password, 'origurl': origUrl}

            r = s.post(loginURL, headers=headers, data=payload, verify=verifySSL)

            if 0:
                print topicUrl
                print r.status_code
                print r.url
                #print r.text
                #print dir(r)
                print r.headers

                print r.cookies
                print r.history
                for resp in r.history:
                    print resp.status_code, resp.url

            soup = BeautifulSoup(r.text)

            # if there's again a login form, we failed to log in
            loginform = soup.find('form', {'name' : "loginform"})
            if loginform:
                print "login failed"
                sys.exit(1)

        if rawMode or printableMode:
            #print "reloading", topicUrl
            r = s.get(topicUrl, verify=verifySSL)

            if r.status_code == 200:
                soup = BeautifulSoup(r.text)

                # if there's again a login form, we failed to log in
                loginform = soup.find('form', {'name' : "loginform"})
                if loginform:
                    print "login failed"
                    sys.exit(1)

        return r.text

def fetchTopicHtmlPrintableView(topicUrl):
    """download a cleaned-up printable view"""
    htmlcontent = fetchTopicHtmlView(topicUrl)

    soup = BeautifulSoup(htmlcontent)

    # get rid of attachments table
    attachs = soup.find("div", class_="twikiAttachments")
    attachs.extract()

    # remove useless stuff
    footer = soup.find("div", class_="patternTopicFooter")
    footer.extract()

    bottombar = soup.find("div", id="patternBottomBar")
    bottombar.extract()

    content = soup.prettify()

    # from tidylib import tidy_document
    
    # content, errors = tidy_document(content)

    # print errors

    return content

def listTopics(htmlcontent):
    topics=[]

    soup = BeautifulSoup(htmlcontent)
    # Warn if topic name != WebTopicList
    #  first word of header/title

    # search for patternTopic div
    content = soup.find("div", class_="patternContent")
    #print content

    #  search for ul
    #   searhc for li containing a links with class twikilink
    for link in content.find_all("a", class_="twikiLink"):
        href=link.get('href')
        #     extract topic name from href of the form '/cgi-bin/twiki/view/Dptinf/Wikidptinf/' + 'topicName' == last part of the path
        topic = posixpath.basename(href)
        if topic not in ReservedTWikiWikiTopics:
            topics.append(topic)

    return topics

def fetchTopicRawView(webTopicURL, rev=None):
    params = {'raw': 'on'}
    if rev:
        params['rev'] = rev
    params = urllib.urlencode(params)

    webTopicURL = webTopicURL + '?' + params
    htmlcontent = fetchTopicHtmlView(webTopicURL)
    #print htmlcontent
    return htmlcontent

def getSourceFromTopic(webTopicURL, rev=None):
    """Downloads the raw TWiki syntax source from the wiki"""
    logging.info("fetching source from %s", webTopicURL)

    htmlcontent = fetchTopicRawView(webTopicURL, rev)

    soup = BeautifulSoup(htmlcontent)
    # Warn if topic name != WebTopicList
    #  first word of header/title

    # search for patternTopic div
    content = soup.find("div", class_="patternContent")
    #print content

    textarea = content.find("textarea")
    return textarea.text

def getRevisionOfTopic(webTopicURL):

    #print 'coin'
    htmlcontent = fetchTopicHtmlView(webTopicURL)
    #print 'pan'
    #print 'htmlcontent', htmlcontent

    soup = BeautifulSoup(htmlcontent)

    revinfo = soup.find("span", class_="patternRevInfo")

    revtext = revinfo.text
    #print "revtext", revtext

    m = re.match(r"r(\d+) - .*", revtext)
    
    rev = m.group(1)

    return int(rev)

def getAttachmentsListFromTopic(webTopicURL):

    htmlcontent = fetchTopicHtmlView(webTopicURL)

    soup = BeautifulSoup(htmlcontent)
    # Warn if topic name != WebTopicList
    #  first word of header/title

    files=[]

    # search for patternTopic div
    content = soup.find("div", class_="twikiAttachments")

    if not content:
        return files

    content = content.find("table", id="twikiAttachmentsTable")
 
    rows = content.find_all("tr")

    for row in rows:
        for a in row.find_all("a"):
            href=a['href']

            m = re.match(r"/cgi-bin/twiki/viewfile/.*\?rev=\d+;filename=(.*)", href)
            if m and m.groups :
                #print 'found', m.group(1), 'in', href
                filename = m.group(1)
                url = urlparse.urljoin(webTopicURL, href)
                files.append((filename, url))
            # else:
            #     print 'problem:', href
    return files

def downloadAttachment(filename, url):
    global session
    
    r = session.get(url, stream=True, verify=verifySSL)
    with open(filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                f.flush()

def printableToText(content):
    import html2text

    h = html2text.HTML2Text()
    h.ignore_links = True
    h.body_width = 0
    return h.handle(content)

