# ================================================================
#
# Python script to convert Twiki files to MediaWiki format.
#
# Ported to Python from a Perl version by Olivier Berger
# ( Source: http://wiki.ittoolbox.com/index.php/Code:Twiki2mediawiki )
#
# Copyright (C) 2006-2008 Authors: Anonymous, Betsy_Maz, bcmfh, Kevin Welker
# Copyright (C) 2014 Olivier Berger and Institut Mines-Telecom
# 
# Updates include the use of code from TWiki::Plugins::EditSyntaxPlugin, 
# a GPL'd Plugin from TWiki Enterprise Collaboration Platform, 
# http://TWiki.org/ written by Peter Thoeny
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Output goes to stdout.
# 
# Todo:
# * Convert from MediaWiki to TWiki.
# * Support additional Twiki tags.
# * Consider getting conversion rules straight from 
#   TWiki::Plugins::EditSyntaxPlugin's rule's page instead of duplicating 
#   them within in this code.
#
# ================================================================

import sys
import re

PREFIX='! '

# List of rules to convert twiki lines to mediawiki, many/most 
# borrowed from TWiki::Plugins::EditSyntaxPlugin.
# 
# See http://twiki.org/cgi-bin/view/Plugins/MediawikiEditSyntaxRegex
#
rules = (

    #
    # Wiki Tags
    #
    # q#s/^%TOC%//g#, # Remove Table of contents
    (r'''^%TOC%''', r''), # Remove Table of contents
    (r'''^%STARTINCLUDE%''', r''),
    (r'''^%STOPINCLUDE%''', r''),
    # #q#s/^%META.*//g#, # Remove meta tags
    (r'''^%META.*''', r''), # Remove meta tags
    # q#s/\!([A-Z]{1}\w+?[A-Z]{1})/$1/g#, # remove ! from Twiki words.
    #  (r'''\!([A-Z]{1}\w+?[A-Z]{1})''', r"""\1"""), # remove ! from Twiki words.

    #
    # Formatting
    #
    # q%s/(^|[\s\(])\*([^ ].*?[^ ])\*([\s\)\.\,\:\;\!\?]|$)/$1'''$2'''$3/g%, # bold
    (r'''(^|[\s\(])\*([^ ].*?[^ ])\*([\s\)\.\,\:\;\!\?]|$)''', r"""\1'''\2'''\3"""), # bold
    # q%s/(^|[\s\(])\_\_([^ ].*?[^ ])\_\_([\s\)\.\,\:\;\!\?]|$)/$1''<b>$2<\/b>''$3/g%, # italic bold
    (r'''(^|[\s\(])\_\_([^ ].*?[^ ])\_\_([\s\)\.\,\:\;\!\?]|$)''', r"""\1''<b>\2</b>''\3"""), # italic bold
    # q%s/(^|[\s\(])\_([^ ].*?[^ ])\_([\s\)\.\,\:\;\!\?]|$)/$1''$2''$3/g%, # italic
    (r'''(^|[\s\(])\_([^ ].*?[^ ])\_([\s\)\.\,\:\;\!\?]|$)''', r"""\1''\2''\3"""), # italic
    # q%s/(^|[\s\(])==([^ ].*?[^ ])==([\s\)\.\,\:\;\!\?]|$)/$1'''<tt>$2<\/tt>'''$3/g%, # monospaced bold
    (r'''(^|[\s\(])==([^ ].*?[^ ])==([\s\)\.\,\:\;\!\?]|$)''', r"""\1'''<tt>\2</tt>'''\3""" ), # monospaced bold
    # q%s/(^|[\s\(])=([^ ].*?[^ ])=([\s\)\.\,\:\;\!\?]|$)/$1<tt>$2<\/tt>$3/g%, # monospaced
    (r'''(^|[\s\(])=([^ ].*?[^ ])=([\s\)\.\,\:\;\!\?]|$)''', r"""\1<tt>\2</tt>\3""" ), # monospaced
    # q%s/(^|[\n\r])---\+\+\+\+\+\+([^\n\r]*)/$1======$2 ======/%, # H6
    # q%s/(^|[\n\r])---\+\+\+\+\+([^\n\r]*)/$1=====$2 =====/%, # H5
    # q%s/(^|[\n\r])---\+\+\+\+([^\n\r]*)/$1====$2 ====/%, # H4
    # q%s/(^|[\n\r])---\+\+\+([^\n\r]*)/$1===$2 ===/%, # H3
    # q%s/(^|[\n\r])---\+\+([^\n\r]*)/$1==$2 ==/%, # H2
    # q%s/(^|[\n\r])----\+\+([^\n\r]*)/$1==$2 ==/%, # H2 (slightly misformed variant)
    # q%s/(^|[\n\r])---\+([^\n\r]*)/$1=$2 =/%, # H1
    (r'''(^|[\n\r])---\+\+\+\+\+\+([^\n\r]*)''', r"""\1======\2 ======"""), # H6
    (r'''(^|[\n\r])---\+\+\+\+\+([^\n\r]*)''', r"""\1=====\2 ====="""), # H5
    (r'''(^|[\n\r])---\+\+\+\+([^\n\r]*)''', r"""\1====\2 ===="""), # H4
    (r'''(^|[\n\r])---\+\+\+([^\n\r]*)''', r"""\1===\2 ==="""), # H3
    (r'''(^|[\n\r])---\+\+([^\n\r]*)''', r"""\1==\2 =="""), # H2
    (r'''(^|[\n\r])----\+\+([^\n\r]*)''', r"""\1==\2 =="""), # H2 (slightly misformed variant)
    (r'''(^|[\n\r])---\+[!]*([^\n\r]*)''', r"""\1=\2 ="""), # H1

    # 
    # Links
    #
    # q%s/\[\[(https?\:.*?)\]\[(.*?)\]\]/\[$1 $2\]/g%, # external link [[http:...][label]]
    (r'''\[\[(https?:.*?)]\[(.*?)]]''', r"""[\1 \2]"""), # external link [[http:...][label]]

    #
    # Bullets
    #
    # q%s/(^|[\n\r])[ ]{3}\* /$1\* /%, # level 1 bullet 
    # q%s/(^|[\n\r])[\t]{1}\* /$1\* /%, # level 1 bullet: Handle single tabs (from twiki .txt files)
    # q%s/(^|[\n\r])[ ]{6}\* /$1\*\* /%, # level 2 bullet 
    # q%s/(^|[\n\r])[\t]{2}\* /$1\*\* /%, # level 1 bullet: Handle double tabs
    # q%s/(^|[\n\r])[ ]{9}\* /$1\*\*\* /%, # level 3 bullet 
    # q%s/(^|[\n\r])[\t]{3}\* /$1\*\*\* /%, # level 3 bullet: Handle tabbed version  
    # q%s/(^|[\n\r])[ ]{12}\* /$1\*\*\*\* /%, # level 4 bullet
    # q%s/(^|[\n\r])[ ]{15}\* /$1\*\*\*\*\* /%, # level 5 bullet
    # q%s/(^|[\n\r])[ ]{18}\* /$1\*\*\*\*\*\* /%, # level 6 bullet
    # q%s/(^|[\n\r])[ ]{21}\* /$1\*\*\*\*\*\*\* /%, # level 7 bullet
    # q%s/(^|[\n\r])[ ]{24}\* /$1\*\*\*\*\*\*\*\* /%, # level 8 bullet
    # q%s/(^|[\n\r])[ ]{27}\* /$1\*\*\*\*\*\*\*\*\* /%, # level 9 bullet
    # q%s/(^|[\n\r])[ ]{30}\* /$1\*\*\*\*\*\*\*\*\*\* /%, # level 10 bullet
    (r'''(^|[\n\r])[ ]{3}\* ''', r"""\1* """), # level 1 bullet 
    (r'''(^|[\n\r])[\t]{1}\* ''', r"""\1* """), # level 1 bullet: Handle single tabs (from twiki .txt files)
    (r'''(^|[\n\r])[ ]{6}\* ''', r"""\1** """), # level 2 bullet 
    (r'''(^|[\n\r])[\t]{2}\* ''', r"""\1** """), # level 1 bullet: Handle double tabs
    (r'''(^|[\n\r])[ ]{9}\* ''', r"""\1*** """), # level 3 bullet 
    (r'''(^|[\n\r])[\t]{3}\* ''', r"""\1*** """), # level 3 bullet: Handle tabbed version  
    (r'''(^|[\n\r])[ ]{12}\* ''', r"""\1**** """), # level 4 bullet
    (r'''(^|[\n\r])[ ]{15}\* ''', r"""\1***** """), # level 5 bullet
    (r'''(^|[\n\r])[ ]{18}\* ''', r"""\1****** """), # level 6 bullet
    (r'''(^|[\n\r])[ ]{21}\* ''', r"""\1******* """), # level 7 bullet
    (r'''(^|[\n\r])[ ]{24}\* ''', r"""\1******** """), # level 8 bullet
    (r'''(^|[\n\r])[ ]{27}\* ''', r"""\1********* """), # level 9 bullet
    (r'''(^|[\n\r])[ ]{30}\* ''', r"""\1********** """), # level 10 bullet

    #
    # Numbering
    #
    # q%s/(^|[\n\r])[ ]{3}[0-9]\.? /$1\# /%, # level 1 bullet
    # q%s/(^|[\n\r])[\t]{1}[0-9]\.? /$1\# /%, # level 1 bullet: handle 1 tab
    # q%s/(^|[\n\r])[ ]{6}[0-9]\.? /$1\#\# /%, # level 2 bullet
    # q%s/(^|[\n\r])[\t]{2}[0-9]\.? /$1\#\# /%, # level 2 bullet: handle 2 tabs
    # q%s/(^|[\n\r])[ ]{9}[0-9]\.? /$1\#\#\# /%, # level 3 bullet
    # q%s/(^|[\n\r])[\t]{3}[0-9]\.? /$1\#\#\# /%, # level 3 bullet: handle 3 tabs
    # q%s/(^|[\n\r])[ ]{12}[0-9]\.? /$1\#\#\#\# /%, # level 4 bullet
    # q%s/(^|[\n\r])[ ]{15}[0-9]\.? /$1\#\#\#\#\# /%, # level 5 bullet
    # q%s/(^|[\n\r])[ ]{18}[0-9]\.? /$1\#\#\#\#\#\# /%, # level 6 bullet
    # q%s/(^|[\n\r])[ ]{21}[0-9]\.? /$1\#\#\#\#\#\#\# /%, # level 7 bullet
    # q%s/(^|[\n\r])[ ]{24}[0-9]\.? /$1\#\#\#\#\#\#\#\# /%, # level 8 bullet
    # q%s/(^|[\n\r])[ ]{27}[0-9]\.? /$1\#\#\#\#\#\#\#\#\# /%, # level 9 bullet
    # q%s/(^|[\n\r])[ ]{30}[0-9]\.? /$1\#\#\#\#\#\#\#\#\#\# /%, # level 10 bullet
    (r'''(^|[\n\r])[ ]{3}[0-9]\.? ''', r"""\1# """), # level 1 bullet
    (r'''(^|[\n\r])[\t]{1}[0-9]\.? ''', r"""\1# """), # level 1 bullet: handle 1 tab
    (r'''(^|[\n\r])[ ]{6}[0-9]\.? ''', r"""\1## """), # level 2 bullet
    (r'''(^|[\n\r])[\t]{2}[0-9]\.? ''', r"""\1## """), # level 2 bullet: handle 2 tabs
    (r'''(^|[\n\r])[ ]{9}[0-9]\.? ''', r"""\1### """), # level 3 bullet
    (r'''(^|[\n\r])[\t]{3}[0-9]\.? ''', r"""\1### """), # level 3 bullet: handle 3 tabs
    (r'''(^|[\n\r])[ ]{12}[0-9]\.? ''', r"""\1#### """), # level 4 bullet
    (r'''(^|[\n\r])[ ]{15}[0-9]\.? ''', r"""\1##### """), # level 5 bullet
    (r'''(^|[\n\r])[ ]{18}[0-9]\.? ''', r"""\1###### """), # level 6 bullet
    (r'''(^|[\n\r])[ ]{21}[0-9]\.? ''', r"""\1####### """), # level 7 bullet
    (r'''(^|[\n\r])[ ]{24}[0-9]\.? ''', r"""\1######## """), # level 8 bullet
    (r'''(^|[\n\r])[ ]{27}[0-9]\.? ''', r"""\1######### """), # level 9 bullet
    (r'''(^|[\n\r])[ ]{30}[0-9]\.? ''', r"""\1########## """), # level 10 bullet
    # q%s/(^|[\n\r])[ ]{3}\$ ([^\:]*)/$1\; $2 /g% # $ definition: term
    (r'''(^|[\n\r])[ ]{3}\$ ([^:]*)''', r"""\1; \2 """), # $ definition: term

    (r'''^[\s]+''', r''),

    (r'''^%SEARCH{[^}]*}%''', r''), # Remove SEARCH macro

    (r'''\[\[%SCRIPTURL{"view"}%/([A-Z][A-Z]*[a-z]+/)+([^\]]+\])''', r'[[\2'), # Remove view links

    (r'''%MAKETEXT{"([^"]+)"}%''', r'\1'), 
    
    (r'''^---$''', r'----'), 
    
    (r'''-- Main\.([\S]+)''', r'-- [[User:\1][\1]]'),  #-- Main.OlivierBerger - 23 Apr 2009

    (r'''<verbatim>(.*)</verbatim>''', r'<pre>\1</pre>'),
)

def translateText(line, topicname='', category=None):
    """Converts a line from TWiki to Mediawiki syntaxes"""

    for r in rules:
        search, replace = r
        line = re.sub(search, replace, line)

    # Convert CamelCase internal links

    # All kinds of hacks since I didn't do proper regex in the first place :-/
    linked = re.sub(r'''\b([A-Z][A-Z]*[a-z]+([^\s/.]*?)[A-Z]([A-Za-z]*?))\b''', r"""[[\1]]""", line)
    if linked != line:
        linked = re.sub(r'''\[{4}''', r"""[[""", linked)
        linked = re.sub(r'''\]{4}''', r"""]]""", linked)
        linked = re.sub(r'''\]\]#\[\[''', r"""#""", linked)
        linked = re.sub(r'''\]\]\]\[''', r"""][""", linked)
        linked = re.sub(r'''\!\[\[([^\]]*)\]\]''', r"""\1""",linked)
        linked = re.sub(r'''<nop>\[\[([^\]]*)\]\]''', r"""\1""",linked)
        linked = re.sub(r'''^#\[\[([^\]]*)\]\]''', r"""<div id="\1"></div>""",linked)
        linked = re.sub(r'''(https?://\S*/)\[\[([^\]]*)\]\](\S*)''', r"""\1\2\3""", linked) # camel case inside a URL
        linked = re.sub(r'''\[\[([^\]]*)\]\]@''', r"""\1@""", linked) # camel case in left part of email address
        linked = re.sub(r'''(\[[^\]]*)\[\[([^\]]*)\]\](.*\])''', r"""\1\2\3""", linked) # camel case inside description of URL
        linked = re.sub(r'''([^\]]*\]\[)([^\[]*?)\[\[([^\]]+)\]\]([^\]]*?)(\]\])''', r"""\1\2\3\4\5""", linked) # camel case inside the right part of an internal link
        linked = re.sub(r'''([A-Z][A-Z]*[a-z]+\.)\[\[([^\]]+)\]\]''', r"""\1\2""", linked)
        #        linked = re.sub(r'''^\[\[%SCRIPTURL{"view"}%/([A-Z][A-Z]*[a-z]+/)+''', r'[[', linked) # Remove view links
        linked = re.sub(r'''\]\[\[\[''', r'][', linked) #    [[TopicName][[[TopicName]]
        linked = re.sub(r'''\]\[''', r'|', linked) 

        line = linked

    line = re.sub(r'''%TOPIC%''', topicname, line)

    result = re.match(r'''(.*)%ATTACHURL%/([^\.]*)(.*)''', line)
    if result:
        basename = result.group(2)
#        basename = basename.replace('_','')
#        basename = basename.replace('-','')
        line = result.group(1) + 'Media:' + topicname + '_' + basename + result.group(3)
    line = re.sub(r'''\[\[(Media:[^\]]*)\]\[([^\]\[]*)\]\]''', r"""[[\1|\2]]""", line)

    if category:
        line = re.sub(r"""(\* ?)\[\[([^\]\|]+)%s[\]\|].*\]\]""" % category, r"""[[Category:\2]]""", line)

    return line

def convertedFromStream(file, topicname='', category=None):
    """Converts a stream from TWiki syntax to Mediawiki's"""

    converted = ''

    convertingTable = False

    convertingVerbatim = False

    for line in file.readlines():
        if line[-1] == "\n":
            line=line[:-1]

        #
        # Handle Table Endings 
        #
        # if ($convertingTable && /^[^\|]/) {  
        #     print ("|}\n\n"); 
        #     $convertingTable = 0;
        # }
        if convertingTable:
            if re.match(r'''^[^\|]''', line):
                converted += "|}\n\n"
                convertingTable = False

        #
        # Handle Tables 
        #  * todo: Convert to multi-line regular expression
        #          as table data doesn't get run through the list of rules currently
        #
        # if (/\|/) {
        if re.match(r'''\|''', line):

            # Is this the first row of the table? If so, add header
            #     if (!$convertingTable) {
            #         print "{| cellpadding=\"5\" cellspacing=\"0\" border=\"1\"\n";
            #         $convertingTable = 1;
            #     }
            if not convertingTable:
                converted += '{| cellpadding="5" cellspacing="0" border="1"' + "\n"
                convertingTable = True

            # start new row
            converted += "|-\n"

            #     my $arAnswer = $_;
            #   $arAnswer =~ s/\|$//; #remove end pipe.
            arAnswer = re.sub(r'''\|$''', r"", line) #remove end pipe.

            #     $arAnswer =~ s/(.)\|(.)/$1\|\|$2/g; #Change single pipe to double pipe.
            #arAnswer = re.sub(r'''(.)\|(.)''', r"\1||\2", arAnswer)

            #        list = re.split(r'\|', arAnswer[1:])
            # print list
            # list = map(translateText, list)
            # print list
            text = '||'.join(map(translateText, re.split(r'\|', arAnswer[1:])))
            #   my $text = _translateText($arAnswer);
            #text = translateText(arAnswer)
            converted +=  '|' + text + "\n"

        #
        # Handle blank lines..
        #
        # } elsif (/^$/) {
        #     print"$_\n";

        elif re.match(r'^$', line):
            #print PREFIX + line
            converted += line + "\n"


        # q#s/<(\/?)verbatim>/<$1pre>/g#,  # update verbatim tag.
        elif convertingVerbatim:
            if re.match(r'''</verbatim>''', line):
                converted += "</nowiki>\n"
                convertingVerbatim = False
                #                print >>sys.stderr, "convertingVerbatim OFF", line, converted
            else:
                converted += line + "\n"
        elif re.match(r'''^<verbatim>''', line):
                converted += re.sub(r'''^<verbatim>''', r""" <nowiki>""", line) + "\n"
                convertingVerbatim = True
                #                print >>sys.stderr, "convertingVerbatim ON", line, converted
    
        #
        # Handle anything else...
        #
        # } else {
        #   my $text = _translateText($_);  
        #   print "$text\n";
        # }

        else:
            text = translateText(line, topicname=topicname, category=category)
            converted += text + "\n"

    # Get rid of the Categories header
    converted = re.sub(r'''----\n<b>Categories for.*</b>''', r'', converted)

    return converted


if __name__ == '__main__':

    filename = sys.argv[1]

    file = open(filename)

    print convertedFromStream(file)

    file.close()
