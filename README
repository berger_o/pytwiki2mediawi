
#+TITLE: TWiki to FusionForge's Mediawiki migration tools


* Principles

This [[https://fusionforge.int-evry.fr/scm/?group_id=53][repository]] contains a set of tools intended to allow migrating
contents of a TWiki wiki to the MediaWiki of a [[http://fusionforge.org][FusionForge]] project.

The interface with the existing TWiki is based on Web scraping of the
Wiki interface, tested with a TWiki 4.0 version, and the pattern skin.

The interface with the destination Mediawiki goes through Mediawiki's
API, but authentication is made to FusionForge's builtin auth plugin
(tested on FusionForge 5.x).

* Context

These tools have been developed in order to ease migration of projects
from PicoForge to FusionForge.

* License

These tools are Free Software. Each script should include its own
copyright & license header.

There's absolutely no waranty of any kind on the behaviour of the
tools: use them at your own risk.

* Tools

** Common features

Access to restricted wikis is possible if they are protected by the
standard TWiki or FusionForge login form (see -u and -p options).

** Caveats

The tools currently operate in brute force mode, without
throttling. Pay attention to not connecting to a server that you don't
own, for you may very well overload it.

** listtopics.py

Will provide a list of topics in the Wiki, by parsing a WebTopicList
topic content.

** backuptopic.py

Will download a copy of the :
 - source of the wiki pages/topics (in TWiki syntax) present in a wiki (saving to .raw files in a subdir)
 - all topic attachments for those topics (into .attachs subdirs)
 - optionally source for all revisions of the topics (see --history
   option)
 - printable HTML versions of the latest version (to be able to attach it to
   the imported page

** import.py

Will allow importing a backup made with backuptopic.py into a
FusionForge project's wiki :
- will import the lates version, converting the syntax
- will import all attachements and recreate an attachments table
- will add the original raw TWiki source and printable HTML in
  attachments of each page
- Will handle Category topic suffixes, i.e. converting TWiki links of
  the form =[[AcategoryCategory]]= to =[[Category:Acategory]]= (with
  --category)

*** fromtwiki.py

This script is an adaptation of an earlier syntax converter originally
written in Perl. It is a hell of a hack, and shoud be unit-testable
with nose (nosetests -s -v tests). Ideally, it should be rewritten by
someone knowing how to use regexes properly... or better with a parser
based on a formal grammar, etc.

* Dependencies

- httpcache (https://pypi.python.org/pypi/httpcache)
- BeautifulSoup 4 (bs4)
- poster
- html2txt (optional for --text option)
- wikitools (may need a fix, see below)

* Usage

** Options

See -h or --help usage notices

** Listing contents of the wiki

#+BEGIN_src sh
python listtopics.py -k -u USER -p PASSWORD -i https://mywiki.example.com/cgi-bin/twiki/view/Wiki/Subwiki/WebTopicList
#+END_src

This will list all topics of a TWiki instance (but the special
WebWhatever-like topics) :

- -k will disable SSL/TLS certs checks
- USER/PASSWORD if the wiki is restricted
- -i URL for the wiki's index page (WebTopicList topic) URL

** Backup

We can then use backuptopic.py together with xargs to backup source of
all topics from the wiki into a directory :

#+BEGIN_src sh
python listtopics.py -k -u USER -p PASSWORD -i https://mywiki.example.com/cgi-bin/twiki/view/Wiki/Subwiki/WebTopicList | \
xargs python backuptopic.py -k -u USER -p PASSWORD -d tests --history --printable --text -b https://mywiki.example.com/cgi-bin/twiki/view/Wiki/Subwiki
#+END_src

where :
- -d sets a destination dir
- --history allows downloading all revisions of the topics
- -b sets the base URL for the wiki
- --printable will save the HTML printable version
- --text will convert that HTML printable version to .txt for later comparison

** Using import.py to import a backup

The =import.py= script will convert a TWiki raw source file's content
and import it as a new page (or writing to another revision on an
existing page) in the target MediaWiki :

$ python import.py WebHome  tests/WebHome.raw

It uses 
- the =fromtwiki.py= converter, which can be tested on stdout :

#+BEGIN_src sh
$ python fromtwiki.py tests/WebHome.raw
#+END_src

- the =fusionforge.py= client which will initialize the wikitools
  client API, while performing the login to FusionForge

*** Fixing wikitools

The =fusionforge.py= script illustrates how to use the MediaWiki API
using the [[https://pypi.python.org/pypi/wikitools][wikitools]]
library, while using the fusionforge credentials to login if the wiki
content is restricted.

The principle is to get the session cookies through fusionforge login
form and then use the same client session to connect to the API.

Note : there is a bug in wikitools 1.1.1 from pypy, which produces
wrong User-Agent for requests : apply the following patch :

#+BEGIN_src
--- api.py.orig	2014-03-25 16:39:02.176199937 +0100
+++ api.py	2014-03-25 16:39:16.080168172 +0100
@@ -75,7 +75,7 @@
 				"Content-Type": "application/x-www-form-urlencoded",
 				"Content-Length": len(self.encodeddata)
 			}
-		self.headers["User-agent"] = wiki.useragent,
+		self.headers["User-agent"] = wiki.useragent
 		if gzip:
 			self.headers['Accept-Encoding'] = 'gzip'
 		self.wiki = wiki
#+END_src

*** Importing into a Fusionforge project's MediaWiki

**** Configure the target MediaWiki :
A number of settings might be configured in =ProjectSettings.php=,
like :

- =$wgEnableUploads = true;= will allow uploads
- =$wgUploadPath = $wgScriptPath."/img_auth.php";= protect access to
  uploaded files (if the wiki is meant to be restricted)
- =$wgStrictFileExtensions = false;= only warn on incorrect extensions ?
- =$wgVerifyMimeType = false;= only warn on unknown mime types
- =$wgFileExtensions = array_merge($wgFileExtensions, array('odp', 'ppt', 'html', 'txt'));= add some extensions, like .txt ?
- =$wgFileBlacklist = array_diff( $wgFileBlacklist, array ('html') );= don't backlist HTML
- =$wgMimeTypeBlacklist = array_diff( $wgMimeTypeBlacklist, array ('text/html') );= don't backlist HTML
- =$wgDisableUploadScriptChecks = true;= don't backlist HTML

The global forge config may need to be adjusted too, to handle images
directory creation by the cronjobs :

#+BEGIN_src
[mediawiki]
enable_uploads = true
#+END_src

**** Make sure img_auth.php is supported by the MediaWiki plugin

In FusionForge <= 5.3, the =img_auth.php= isn't handled by the
permission system. LocalSettings.php and the Apache aliases of the
FusionForge mediawiki plugin need to be adjusted to handle
=img_auth.php= the same way as =index.php=.


* TODOS

** Rewrite fromtwiki.py so that it's not a dirty hack

* Changelog

** Version 1.0 <2014-04-07 lun.>

# Local Variables:
# mode: org
# End:
