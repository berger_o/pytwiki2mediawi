
# Convert backup elements from TWiki to MediaWiki and import them into FusionForge

# Copyright 2014 Olivier Berger <olivier.berger@telecom-sudparis.eu> and Institut Mines-Telecom

# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Dependencies : wikitools + poster

import sys
import os.path
import urllib
import argparse

from wikitools import page, api, wikifile

from fromtwiki import convertedFromStream

from fusionforge import initSite, getHtmlPage

from utils import writeHtmlToDir, writeTextToDir
from twiki import printableToText

class UnwantedFileTypeAttachmentException(Exception):
    def __init__(self, args):
        self.args = args
    def __str__(self):
        return "File extension %s not in authorized ones : %s" % (self.args[0], self.args[1])

def uploadAttachment(site, topicname, attachname, filepath, filepagename=None):
    """Upload a file as attachment to a page""" 
    with open(filepath, "rb") as attachfile:
        #            filename = filename.replace('_','')
        #            filename = filename.replace('-','')
        if not filepagename:
            filepagename = 'File:' + topicname + '_' + attachname
        #print "filepagename:", filepagename
        try:
            wikipage = wikifile.File(site, filepagename)
        except page.BadTitle:
            filepagename = urllib.unquote(filepagename)
            filepagename = filepagename.decode('ascii', errors='ignore')
            wikipage = wikifile.File(site, filepagename)
        #print "uploading", attachname, "to", filepagename
        if 1:
#        try:

            #result = wikipage.upload(fileobj=attachfile, ignorewarnings=True)
            jsonreturn = wikipage.upload(fileobj=attachfile)
            jsonreturn = jsonreturn['upload']
            #print jsonreturn
            result = jsonreturn['result']
            if result == 'Warning':
                warnings = jsonreturn['warnings']
                if 'filetype-unwanted-type' in warnings:
                    raise UnwantedFileTypeAttachmentException(warnings['filetype-unwanted-type'])

        return filepagename

        # except api.APIError, err:
        #     cause, message = err
        #     if cause == 'verification-error' and message == 'This file did not pass file verification':
        #         base, extension = os.path.splitext(attachname)
        #         filepagename = 'File:' + topicname + '_' + base + '_' + extension[1:] + '.txt'
        #         wikipage = wikifile.File(site, filepagename)
        #         print "uploading again", attachname, "to", filepagename
        #         #result = wikipage.upload(fileobj=attachfile, ignorewarnings=True)
        #         result = wikipage.upload(fileobj=attachfile)
        #         print result
        #     else:
        #         raise api.APIError, err

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Import into FusionForge's mediawiki a backup of a TWiki site.")
    parser.add_argument('topicname', metavar='TOPICNAME', help='NAME of the TWikiTopic')
    parser.add_argument('filename', metavar='FILENAME', help='NAME of the raw file to be imported')
    parser.add_argument('-a', '--attachments', action="store_true", help='upload all attachments')
    parser.add_argument('--category', help="category suffix")
    parser.add_argument('-b', '--base', required=True, help="base URL for the project's Wiki")
    parser.add_argument('-u', '--user', required=True, help='FusionForge user name')
    parser.add_argument('-p', '--password', required=True, help='FusionForge user password')

    args = parser.parse_args()

    upload_attachments = False
    if args.attachments:
        upload_attachments = True

    category_suffix = None
    if args.category:
        category_suffix = args.category

    mwikiUrl = ''
    if args.base:
        mwikiUrl = args.base
        if mwikiUrl[-1:] != '/':
            mwikiUrl += '/'

    if args.user:
        fusionforge_user = args.user

    if args.password:
        fusionforge_pass = args.password

    topicname, filename = args.topicname, args.filename

    directory = os.path.dirname(filename)

    # Connect to FusionForge project's wiki
    site = initSite(mwikiUrl, fusionforge_user, fusionforge_pass)

    # Upload raw file as attachment
    rawattachname = topicname + '.raw'
    rawfilepagename = topicname + '_raw.txt'
    uploadAttachment(site, topicname, rawattachname, filename, filepagename = 'File:' + rawfilepagename)

    # Add pointer to raw and printable in the header together with "imported" message
    mwcontent = "''This page was converted from a TWiki topic named %s (" % topicname

    printablehtmlattachname = topicname + '-printable.html'
    printablehtmlfilename = os.path.join(directory, printablehtmlattachname)
    #print 'printablehtmlfilename', printablehtmlfilename
    if os.path.isfile(printablehtmlfilename):
        printablehtmlpagename = 'File:' + printablehtmlattachname
        uploadAttachment(site, topicname, printablehtmlattachname, printablehtmlfilename, filepagename=printablehtmlpagename)

        mwcontent += "[[Media:%s|printable HTML]] / " % printablehtmlattachname
        
    mwcontent += "[[Media:%s|source]])''\n----\n" % rawfilepagename

    # if the topic ends with the category suffix
    if category_suffix and topicname.endswith(category_suffix):
        topicname = 'Category:' + topicname[:len(topicname)-len(category_suffix)]

    print "importing", filename, 'in', topicname

    # Convert syntax
    sourcefile = open(filename)
    mwcontent += convertedFromStream(sourcefile, topicname=topicname, category= category_suffix or None)
    sourcefile.close()

    # Upload attachments
    attachmentstable = ''

    if upload_attachments:
        # if there's a .attachs dir
        isdir = os.path.isdir(os.path.join(directory, topicname+'.attachs'))
        #print isdir
        if isdir:
            # list contents
            directory = os.path.join(directory, topicname+'.attachs')
            # for every file, upload it
            for filename in os.listdir(directory):
                if (filename == topicname + '-new.html') or (filename == topicname + '-new.txt') :
                    continue
                attachname = filename
                filepath = os.path.join(directory, filename)
                filepagename = uploadAttachment(site, topicname, attachname, filepath)
                filepagename = filepagename.replace('File:', 'Media:')
                attachmentstable += '* [[%s|%s]]\n' % (filepagename, attachname)

    # Add attachment table
    if attachmentstable:
        mwcontent += """----
''<b>Attachments:</b>''
"""
        #print attachmentstable
        mwcontent += attachmentstable.encode('utf-8')

    # Post the contents of the page to the wiki
    newarticle = page.Page(site,title=topicname)
    result = newarticle.edit(title=topicname, text=mwcontent, summary="test summary for use of api to create pages")
    #print result
    success = result['edit']['result']

    # Get the URL
    pageid = result['edit']['pageid']

    # define the params for the query
    params = {'action':'query', 'pageids': pageid, 'prop': 'info', 'inprop': 'url'}
    # create the request object
    request = api.APIRequest(site, params)
    # query the API
    result = request.query()
    #print result

    fullurl = result['query']['pages'][str(pageid)]['fullurl']
    #print fullurl

    # Fetch the printable version to be able to save it for later comparison
    printableurl = fullurl.replace('index.php/', 'index.php?title=')
    printableurl += '&printable=yes'

    #print printableurl

    htmlcontent = getHtmlPage(printableurl)

    writeHtmlToDir(htmlcontent, directory, topicname, ext='-new.html')

    htmlcontent = htmlcontent.decode('utf-8')
    # replace non-breaking spaces by single spaces
    htmlcontent = htmlcontent.replace(unichr(160), " ")
    #print htmlcontent
    
    textcontent = printableToText(htmlcontent)
    writeTextToDir(textcontent, directory, topicname, ext='-new.txt')


    # if success == 'Success':
    #     sys.exit(0)
    # else:
    #     print >> sys.stderr, 'Error:', result
    #     sys.exit(1)
