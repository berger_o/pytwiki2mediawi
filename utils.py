
# Copyright 2014 Olivier Berger <olivier.berger@telecom-sudparis.eu> and Institut Mines-Telecom

# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os.path

def writeHtmlToDir(source, dir, topicname, ext='.html'):
    """Writes the HTML rendering of a Topic (potentially the "printable" version) to a directory."""
    outputfilename = topicname + ext

    outputfilename = os.path.join(dir, outputfilename)
    #print 'wrote', outputfilename

    outputfile = open(outputfilename, 'wb')
    outputfile.write(source.encode('utf8'))
    outputfile.close()

def writeTextToDir(source, dir, topicname, ext='.txt'):
    """Writes a textual rendering of a Topic to a directory."""
    outputfilename = topicname + ext

    outputfilename = os.path.join(dir, outputfilename)
    #print 'wrote', outputfilename

    outputfile = open(outputfilename, 'wb')
    outputfile.write(source.encode('utf8'))
    outputfile.close()

