
# Listing topics in a TWiki wiki

# Copyright 2014 Olivier Berger <olivier.berger@telecom-sudparis.eu> and Institut Mines Telecom

# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import sys
from bs4 import BeautifulSoup


from twiki import listTopics, ReservedTWikiWikiTopics, fetchTopicHtmlView
import twiki

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract topics list from a TWiki wiki.')
    parser.add_argument('-f', '--file', help='read from a HTML file')
    parser.add_argument('-i', '--index', help='URL of the WebTopicList')
    parser.add_argument('-u', '--user', help='TWiki user name')
    parser.add_argument('-p', '--password', help='TWiki user password')
    parser.add_argument('-k', '--insecure', action="store_true", help='bypass SSL verification')

    args = parser.parse_args()
    if args.file:
        filename = args.file
        htmlfile = open(filename)
        htmlcontent = htmlfile.read()
        #print htmlcontent
        
        print listTopics(htmlcontent)

    if args.user:
        twiki.user = args.user

    if args.password:
        twiki.password = args.password

    verifySSL = True
    if args.insecure:
        twiki.verifySSL = False
        
    if args.index:
        webTopicListURL = args.index
        htmlcontent = fetchTopicHtmlView(webTopicListURL)
        for i in listTopics(htmlcontent):
            print i
