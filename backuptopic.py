
# Download a Topic from a TWiki wiki, with optional attachments and history

# Copyright 2014 Olivier Berger <olivier.berger@telecom-sudparis.eu> and Institut Mines-Telecom

# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import sys
from bs4 import BeautifulSoup
import urlparse
import posixpath
import os
import os.path
import logging
import requests

from twiki import getSourceFromTopic, getRevisionOfTopic, getAttachmentsListFromTopic, downloadAttachment, fetchTopicHtmlView, printableToText, fetchTopicHtmlPrintableView
import twiki

from utils import writeHtmlToDir, writeTextToDir

def writeSourceToDir(source, dir, topicname, rev=None):
    """Writes the "raw" source from a Topic to a .raw file in the specified directory."""
    outputfilename = topicname + '.raw'
    if rev:
        outputfilename += '_%d' % rev
    outputfilename = os.path.join(dir, outputfilename)
    #print 'wrote', outputfilename

    outputfile = open(outputfilename, 'wb')
    outputfile.write(source.encode('utf8'))
    outputfile.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download history of a TWiki topic.')
    parser.add_argument('urls', metavar='URL', nargs='+', help='URL of the WebTopic')
    parser.add_argument('-d', '--dir', help='directory to backup to')
    parser.add_argument('-b', '--base', help='base URL for the Wiki topics')
    parser.add_argument('--history', action="store_true", help='download all revisions')
    parser.add_argument('--html', action="store_true", help='download the latest HTML')
    parser.add_argument('--printable', action="store_true", help='download the latest printable HTML')
    parser.add_argument('--text', action="store_true", help='download a text/markdow, version of the latest printable HTML')
    parser.add_argument('-u', '--user', help='TWiki user name')
    parser.add_argument('-p', '--password', help='TWiki user password')
    parser.add_argument('-k', '--insecure', action="store_true", help='bypass SSL verification')
    parser.add_argument('-v', '--verbose', help='log various operations', action = 'append_const', const = 1)

    args = parser.parse_args()

    if args.user:
        twiki.user = args.user

    if args.password:
        twiki.password = args.password

    verifySSL = True
    if args.insecure:
        twiki.verifySSL = False

    if args.verbose:
        args.verbose = sum(args.verbose)
        logger = logging.getLogger()
        if args.verbose == 0:
            level = logging.WARN
        elif args.verbose == 1:
            level = logging.INFO
        elif args.verbose >= 2:
            level = logging.DEBUG
        print level
        logging.basicConfig(level=level)

    if args.urls:

        if not args.dir:
            print "need directory to write to"
            sys.exit(1)
        dir = args.dir

        history = False
        if args.history:
            history = True

        html = False
        if args.html:
            html = True

        printable = False
        if args.printable:
            printable = True

        toText = False
        if args.text:
            toText = True

        base = ''
        if args.base:
            base = args.base
            if base[-1:] != '/':
                base += '/'

        for webTopicURL in args.urls:

            if not base:
                path = urlparse.urlparse(webTopicURL)
                path = path.path
                topicname = posixpath.basename(path)
            else:
                topicname = webTopicURL
                webTopicURL = urlparse.urljoin(base, topicname)

            # print topicname, webTopicURL
            # continue

            if html == True:
                htmlcontent = fetchTopicHtmlView(webTopicURL)

                writeHtmlToDir(htmlcontent, dir, topicname)

            elif printable == True:
                printableWebTopicURL = webTopicURL + '?template=viewprint'
                #print 'saving', printableWebTopicURL
                htmlcontent = fetchTopicHtmlPrintableView(printableWebTopicURL)

                writeHtmlToDir(htmlcontent, dir, topicname, ext='-printable.html')

                if toText:
                    textcontent = printableToText(htmlcontent)
                    writeTextToDir(textcontent, dir, topicname)

            source=getSourceFromTopic(webTopicURL)

            writeSourceToDir(source, dir, topicname)
            #        print source

            #session = None

            if history:
                currev = getRevisionOfTopic(webTopicURL)

                for i in range(currev-1, 0, -1):
                    source=getSourceFromTopic(webTopicURL, i)
                    writeSourceToDir(source, dir, topicname, i)

            # then, download attachments
            attachments = getAttachmentsListFromTopic(webTopicURL)
            if attachments:
                attachdir = os.path.join(dir, topicname+'.attachs')
                if not os.path.isdir(attachdir) :
                    os.mkdir(attachdir)
                for filename, url in attachments:
                    filename = os.path.join(attachdir, filename)
                    downloadAttachment(filename, url)

