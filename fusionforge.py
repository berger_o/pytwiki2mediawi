
# Demonstration of API calls on a MediaWiki of a FusionForge Project

# Copyright 2014 Olivier Berger <olivier.berger@telecom-sudparis.eu> and Institut Mines Telecom

# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import urllib
import urllib2
import urlparse
# import sys
from bs4 import BeautifulSoup
# import json

from wikitools import wiki, api, pagelist, page

site = None

def loginUrl(mediawikibase):
    """Returns the FusionForge login URL for a project's base mediawiki URL"""
    parsed=urlparse.urlparse(mediawikibase)
    indexpage=urlparse.urljoin(parsed.path, 'index.php')
    indexpage = urllib.quote_plus(indexpage)
    login = (parsed.scheme, parsed.netloc, '/account/login.php','', 'return_to=/', '')
    loginurl=urlparse.urlunparse(login)
    return loginurl

def performFusionForgeLogin(opener, loginurl, username, password):
    """ """
    request = urllib2.Request(loginurl) #, self.encodeddata, self.headers)
    data = opener.open(request)
    #response = data.info()
    #print response
    htmlcontent = data.read()
    #print htmlcontent

    # Find the login form in the login page
    soup = BeautifulSoup(htmlcontent)

    # extract bits from the login form
    maindiv = soup.find('div', id='maindiv')

    loginform = maindiv.find('form')
    action = loginform['action']
    #print action

    inputtag = loginform.find('input', attrs={'name': 'form_key', 'type':'hidden'})
    formkeyval = inputtag['value']
    #print formkeyval

    inputtag = loginform.find('input', attrs={'name': 'return_to', 'type':'hidden'})
    returntoval = inputtag['value']
    #print returntoval

    # prepare the form to post
    data = {
        'form_key': formkeyval,
        'form_loginname': username,
        'form_pw': password,
        'login':	'Login',
        'return_to': returntoval}

    encodeddata = api.urlencode(data,1)

    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Content-Length": len(encodeddata),
        #        'User-agent': 'python-wikitools/1.1.1'
    }

    # post the form

    request = urllib2.Request(action, encodeddata, headers)


    data = opener.open(request)
    #response = data.info()
    #    print response
    #htmlcontent = data.read()
    #print htmlcontent



def initSite(mwikiUrl, fusionforge_user, fusionforge_pass):

    # create a Wiki object
    global site

    if not site:
        site = wiki.Wiki(urlparse.urljoin(mwikiUrl, "api.php") )
        #    print site.useragent

    try:
        site.setSiteinfo()
    except api.APIError, err: 
        cause, message = err
        if cause == 'readapidenied' : # 'You need read permission to use this module'

            # FIRST, login to fusionforge

            # We use the urllib2 opener with the same cookies jar as in wikitools 
            #opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(site.cookies), urllib2.HTTPSHandler(debuglevel=1))
            opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(site.cookies))

            # Use the same User-Agent as wikitools later, so that the session matches
            opener.addheaders = [('User-agent', site.useragent)]

            loginurl=loginUrl(mwikiUrl)

            performFusionForgeLogin(opener, loginurl, fusionforge_user, fusionforge_pass)

            # THEN proceed to Mediawiki API calls

            #site.cookies.save(site,'coin', True, True)

            # now if the login was successful, we can reset the site infos
            site.setSiteinfo()

        else:
            raise api.APIError, err

        return site

    # # define the params for the query
    # #params = {'action':'query', 'titles':'Main Page'}
    # #params = {'action':'query', 'titles':'Accueil'}
    # # create the request object
    # request = api.APIRequest(site, params)
    # # query the API
    # result = request.query()
    # print result
    # if result['login']['result']=='NeedToken':
    #     token = result['login']['token']

    #     params['lgtoken'] = token

    #     request = api.APIRequest(site, params)
    #     # query the API
    #     result = request.query()

    #     print result
    # else:
    #     print result

    # # define the params for the query
    # params = {'action':'query', 'titles':'Main_Page'}
    # # create the request object
    # request = api.APIRequest(site, params)
    # # query the API
    # result = request.query()
    # print result

    # #print pagelist.listFromTitles(site, ('Main_Page',))

    # params = {'action': 'query', 'meta': 'siteinfo', 'maxlag': '120', 'siprop': 'general|namespaces|namespacealiases', 'format': 'json'}

    # request = api.APIRequest(site, params)
    # # query the API
    # result = request.query()
    # print result

def getHtmlPage(url):
    """extract the mediawiki part from the fusionforge html page"""
    global site

    request = urllib2.Request(url)

    #opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(site.cookies), urllib2.HTTPSHandler(debuglevel=1))
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(site.cookies))
    opener.addheaders = [('User-agent', site.useragent)]

    data = opener.open(request)

    htmlcontent = data.read()

    soup = BeautifulSoup(htmlcontent)
    
    headert = soup.find("table", id="header")
    headert.extract()

    tabgeneratort = soup.find("table", class_='tabGenerator')
    tabgeneratort.extract()

    tabgeneratort = soup.find("table", class_='tabGenerator')
    tabgeneratort.extract()

    # FusionForge footer (includes logo)
    footer = soup.find("div", class_="align-right")
    if footer:
        footer.extract()

    mwfooter = soup.find("div", id="footer")
    mwfooter.extract()
    
    h1 = soup.find("h1")
    h1.extract()

    h1 = soup.find("h1")
    h1.extract()

    sitesub = soup.find("div", id="siteSub")
    sitesub.extract()

    toct = soup.find("table", id="toc")
    if toct:
        toct.extract()

    jumptonav = soup.find("div", id="jump-to-nav")
    jumptonav.extract()

    columnone = soup.find("div", id="column-one")
    columnone.extract()

    printfooter = soup.find("div", class_="printfooter")
    printfooter.extract()
    return soup


if __name__ == '__main__':

    parser.add_argument('-b', '--base', required=True, help="base URL for the project's Wiki")
    parser.add_argument('-u', '--user', required=True, help='FusionForge user name')
    parser.add_argument('-p', '--password', required=True, help='FusionForge user password')

    args = parser.parse_args()

    mwikiUrl = ''
    if args.base:
        mwikiUrl = args.base
        if mwikiUrl[-1:] != '/':
            mwikiUrl += '/'

    if args.user:
        fusionforge_user = args.user

    if args.password:
        fusionforge_pass = args.password

    site = initSite(mwikiUrl, fusionforge_user, fusionforge_pass)

    newarticle = page.Page(site,title="Test Page")
    newarticle.edit(title="Test Page", text="This is some text text", summary="test summary for use of api to create pages")
